#-*- coding:UTF-8 -*-

#
# 
# [     Zi     ] https://bitbucket.org/XKY/zi
# - clean.py
# 
#  Date: 2014.06.21
#  License: LGPL v3
# 
#

#================================================================================
#
# clean.py
# - 불필요한 파일 제거( 원격 서버에 push하기 전에 실행 )
#
#================================================================================

# modules
import os

if __name__ == "__main__":
	src_path = "./source/"

	# 불필요한 파일 목록
	remove_files = [
		src_path + "database/dbp.db",
		src_path + "database/post.db",
		src_path + "database/system.db"
	]

	# 파일 제거
	for obj in range( 0, len(remove_files)-1 ):		
		try:
			os.unlink( remove_files[obj] )
		except:
			print "[ERROR]", remove_files[obj]
	
	print "---------- ALL DONE! ----------"
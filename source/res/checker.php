﻿<html>
	<head>
		<meta charset="UTF-8"/>
		<title>check</title>
	</head>
	
	<body>
		<header>
			<h1>Checker</h1>
		</header>
		
		<section>
			<?php
			$class = [
				"SQLite3"
			];
			
			$function = [
				"sqlite_open",
				"json_decode",
				"gd_info",
				"file_get_contents"
			];
			
			$names = array(
				"sqlite_open" => "SQLite2",
				"json_decode" => "JSON",
				"gd_info" => "GD",
				"file_get_contents" => "FileSocket",
				"SQLite3" => "SQLite3",
			);
			
			echo("<h2>Class</h2>");
			foreach( $class as $element ){
				echo( "$element: " );
				if( class_exists($element) )
				{ echo("<font color='blue'>".$names[$element]."</font><br>"); }
				else
				{ echo("<font color='red'>".$names[$element]."</font><br>"); }
			}
			
			echo("<h2>Function</h2>");
			foreach( $function as $element ){
				echo( "$element: " );
				if( function_exists($element) )
				{ echo("<font color='blue'>".$names[$element]."</font><br>"); }
				else
				{ echo("<font color='red'>".$names[$element]."</font><br>"); }
			}
			?>
		</section>
	</body>
</html>
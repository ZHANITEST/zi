<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Setup</title>
	</head>
	
	<body>
	<?php
	/*
	 * 
	 * [     Zi     ] https://bitbucket.org/XKY/zi
	 * - setup_ok.php
	 * 
	 *  Date: 2014.06.21
	 *  License: LGPL v3
	 * 
	 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
	 * ░░░░░░░░░░░░░▄▄▄▄▄▄▄░░░░░░░░░
	 * ░░░░░░░░░▄▀▀▀░░░░░░░▀▄░░░░░░░
	 * ░░░░░░░▄▀░░░░░░░░░░░░▀▄░░░░░░
	 * ░░░░░░▄▀░░░░░░░░░░▄▀▀▄▀▄░░░░░
	 * ░░░░▄▀░░░░░░░░░░▄▀░░██▄▀▄░░░░
	 * ░░░▄▀░░▄▀▀▀▄░░░░█░░░▀▀░█▀▄░░░
	 * ░░░█░░█▄▄░░░█░░░▀▄░░░░░▐░█░░░
	 * ░░▐▌░░█▀▀░░▄▀░░░░░▀▄▄▄▄▀░░█░░
	 * ░░▐▌░░█░░░▄▀░░░░░░░░░░░░░░█░░
	 * ░░▐▌░░░▀▀▀░░░░░░░░░░░░░░░░▐▌░
	 * ░░▐▌░░░░░░░░░░░░░░░▄░░░░░░▐▌░
	 * ░░▐▌░░░░░░░░░▄░░░░░█░░░░░░▐▌░
	 * ░░░█░░░░░░░░░▀█▄░░▄█░░░░░░▐▌░
	 * ░░░▐▌░░░░░░░░░░▀▀▀▀░░░░░░░▐▌░
	 * ░░░░█░░░░░░░░░░░░░░░░░░░░░█░░
	 * ░░░░▐▌▀▄░░░░░░░░░░░░░░░░░▐▌░░
	 * ░░░░░█░░▀░░░░░░░░░░░░░░░░▀░░░
	 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     * 
	 */
	$BackToTheTopJS = "<a href='javascript:history.back(-1)'>뒤로</a>";
        
	$Nicname = $_POST["NICNAME"];
	$Email = $_POST["EMAIL"];
	$Password = $_POST["PASSWORD"];
	$DatabaseType = $_POST["DBTYPE"];
	
	//
	// Install Start
	// 접근 확인( + DBTYPE 검사 )
	if( $Nicname && $Email && $Password && $DatabaseType && ($DatabaseType == 2 || $DatabaseType == 3) ){
            // 비밀번호 길이 검사
            if( strlen($Password)  <= 6 ) { echo("비밀번호가 너무 짧습니다.<br>"); echo($BackToTheTopJS); exit(); }
		
		// 데이터배열 작성
				// ====================== 참고 =========================
                // [0]: Nicname [1]:Email [2]:Password [3]:DatabaseType
				// =====================================================
		$list = array( $Nicname, $Email, $Password, $DatabaseType);
		
		// 배열 디버그 프린터
		require("zi.php");
		debug_echo( $list );
		
		// HTML 스크립트 필터링
		for($i=0; $i<>count($list); $i++){
			$list[$i] = htmlspecialchars( $list[$i] );
		}
		
		// 배열 재배치 + 해쉬화 + 리스트 제거
		$Nicname=$list[0]; $Email=$list[1]; $Password=md5($list[2]); $DatabaseType=$list[3];
		unset($list);
		
			
		// 디렉토리가 없을 경우 만들기 & 있을 경우 초기화
		if(!file_exists("database") ){ mkdir("database"); } else { @unlink("./database/engine.db"); }

		// ***엔진 db 작성 & 새 테이블
		$EngineDatabase = new Database("./database/engine.db", $DatabaseType);
		$EngineDatabase->NewTable( "ADMIN", "id TEXT, pw TEXT, email TEXT");
		$EngineDatabase->InsertValues( "\"$Nicname\", \"$Password\", \"$Email\"");
		$EngineDatabase->Close(); unset( $EngineDatabase );
		
		// ***URL 재구현 DB 작성
		$UrlDatabase = new Database( "./database/url.db", $DatabaseType );
		$UrlDatabase->NewTable( "URLS", "name TEXT" );
		$UrlDatabase->Close(); unset( $UrlDatabase );

		// ***[설정화일]데이터베이스 타입 저장
		conf_new("./database/__conf", "DatabaseType:".$DatabaseType );
		conf_add("./database/__conf", "EnginePath:".get_path() );
		
		echo("설치를 완료하였습니다.<br>");
	}
	else{
		echo("잘못된 접근입니다. 다시 설치페이지로 이동하여 설치를 진행해주세요.<br>"); echo($BackToTheTopJS); exit();
	}
	?>
	</body>
</html>

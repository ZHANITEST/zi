<?php
/*
 * 
 * [     Zi     ] https://bitbucket.org/XKY/zi
 * - index.php
 * 
 *  Date: 2014.06.21
 *  License: LGPL v3
 * 
 */

// 데이터베이스가 없다면 설치화면으로..
if( !file_exists("./database/engine.db") )
{ header("Location:./setup.php"); exit(); }

// 데이터베이스가 발견되면?
else
{
	require("zi.php");
	
	// 이동할 페이지 고유 주소 가져오기
	$Page = @$_GET["page"];
}
?>
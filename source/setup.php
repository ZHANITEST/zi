<!doctype HTML>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Setup</title>
		<link rel="shortcut icon" href="./res/zi_favicon.ico" />
	</head>
	<body>
		<h1>Zi 설치</h1>
		<?php
		/*
		 * 
		 * [     Zi     ] https://bitbucket.org/XKY/zi
		 * - setup.php
		 * 
		 *  Date: 2015.01.03
		 *  License: LGPL v3
		 * 
		 */
		// 에러 표시
		ini_set('display_errors', 1);
		ini_set('error_reporting', E_ALL);
		echo('설치하시려면 아래 내용을 읽고 입력해주세요...<br>');
		echo('혹은 <a href="./res/checker.php">의존성 검사</a>를 해보실 수도 있습니다.<br>');
		?>
 		<br><br>

 		<form method="POST" action="setup_ok.php">
			<textarea cols=100% rows=8>Zi를 사용함으로써 생기는 피해는 개발자가 책임지지 않으며, 소스코드는 LGPL v3에 따라 관리됩니다. 라이선스 원문의 복사본은 Zi와 함께 압축되어 배포되는 lgpl_v3.txt를 참고하시거나 http://opensource.org/licenses/gpl-3.0.html 에서 원문을 보실 수 있습니다. 한글로 요약된 내용은 http://www.olis.or.kr/ossw/license/license/detail.do?lid=1073 에서 보실 수 있습니다.</textarea>
			<br><br>
			닉네임 : <input type="text" name="NICNAME" /> / E-MAIL: <input type="text" name="EMAIL" /><br>
			관리자 비밀번호: <input type="password" name="PASSWORD"><br>
			<font color="red">*<b>7자 이상</b>이여야 하며, <b>특수문자가 조합된 비밀번호가 더 안전</b>합니다.<br>수시로 필요하니 꼭 기억해두세요.</font>
			<br><br>
			<b>..라이선스 내용에 동의하며, 입력한 내용에 따라 설치를 진행하시겠습니까?</b>
			<br>
			<?php
			//
			// can't i use SQLITE2 or 3?
			//
			$DatabaseSelectMessage = "<input type='hidden' name='DBTYPE' value='%d'/><input type='submit' value='SQLite%d로 설치!'/>";
			if( class_exists("SQLite3")){
				echo (   ( sprintf( $DatabaseSelectMessage, 3,3) )   );
			// SQLITE2?
			} else if( function_exists("sqlite_open")){
				echo (   ( sprintf( $DatabaseSelectMessage, 2,2) )   );
			} else {
			// couldn't install.
 			echo("<b>사용할 수 있는 데이터베이스(sqlite3, sqlite2)가 없습니다.. T_T</b>!");
			}
			?>
		</form>
	</body>
</html>
﻿<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" lang="ko" />
		<title>Zi</title>
	</head>
	
	<body>
	<?php
	/*
	 * 
	 * [     Zi     ] https://bitbucket.org/XKY/zi
	 * - admin.php
	 * 
	 *  Date: 2015.02.04
	 *  License: LGPL v3
	 * 
	 */



	// 이메일 주소와 패스워드 부터 얻기, isset으로 선언 됬는 지 확인.
	$Email		= isset( $_POST["email"]		) ? $_POST["email"]		: false ;
	$Password	= isset( $_POST["password"]		) ? $_POST["password"]	: false ;
	require("zi.php");
	
	// 전달된 값이 없다면, 로그인 화면 출력
	if( !$Email or !$Password ){ html_render( "./template/page_login.html"); }
	else if( $Email and $Password ){
		// 존재하다면 데이터베이스 타입 읽기(자동으로 file_exists 검사함.)
		$DatabaseType = conf_get("./database/__conf", "DatabaseType");
		// 데이터베이스 타입의 자료형 검사
		if( $DatabaseType and (int)$DatabaseType ){
			//
			//***엔진의 데이터베이스 읽기 시작 & 테이블 선택
			$EngineDatabase = new Database("./database/engine.db", (int)$DatabaseType );
			$EngineDatabase->SelectTable("ADMIN");
			
			//
			// 차례대로 아이디, 이메일주소, 비밀번호 가져오기
			$Datas["id"] = $EngineDatabase->Get("id");
			$Datas["pw"] = $EngineDatabase->Get("pw");
			$Datas["email"] = $EngineDatabase->Get("email");
			
			//
			// 이메일과 비밀번호가 일치할 때
			if( md5($Password) == $Datas["pw"] and $Email == $Datas["email"] ){
				template_render( "./template/page_config.html", $Datas );
			}
			// 일치하지 않을 때
			else
			{
				echo( "이메일 혹은 비밀번호가 일치하지 않습니다.<br>");
				echo("<a href='javascript:history.back(-1)'>뒤로</a>");
			}
		}
		// 옳바르지 않다면
		else{ die("데이터베이스 타입을 확인할 수 없습니다. T_T"); exit(); }
	}
	?>
	
	</body>
</html>
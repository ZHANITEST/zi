<!DOCTYPE html>
<?php
/*
 * 
 * [     Zi     ] https://bitbucket.org/XKY/zi
 * - zi.php
 * 
 *  Date: 2014.06.21
 *  License: LGPL v3
 * 
 * ⊂_ヽ 
 * 　 ＼＼ Λ＿Λ 
 *　 　 ＼( 'ㅅ') 두둠칫 
 *　　 　 >　⌒ヽ 
 *　　 　/ 　 へ＼ 
 * 　　 /　　/　＼＼ 
 *　 　 ﾚ　ノ　　 ヽ_つ 
 * 　　/　/ 두둠칫 
 * 　 /　/| 
 * 　(　(ヽ 
 * 　|　|、＼ 
 * 　| 丿 ＼ ⌒) 
 * 　| |　　) / 
 *(`ノ )　　Lﾉ
 * 
 */



//
// Delicious Humming
//
const DEBUG = false;



//*****************************************************************************
//*
//*
//*   디버그 관련 클래스, 함수
//*
//*
//*****************************************************************************
//=========================================================
//# func; 디버그 프린트
//=========================================================
function debug_puts( $something, $caption="-" ){
    echo( "[".$caption."]PRINT: <font color='red'><strong>".$something."</strong></font><br>" );
}

//=========================================================
//# func; 디버그 모드 활성화
//=========================================================
function debug()
{
	/*
	ini_set('display_errors', 1);
	ini_set('error_reporting', E_ALL);*/
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}

//=========================================================
//# func; print_r 재구성
//=========================================================
function debug_echo( $mix, $caption = ""){
	echo("<b>".$caption."</b>");
	echo("<pre>"); print_r($mix); echo("</pre>");
}



//*****************************************************************************
//*
//*
//*   에러메시지 HTML 템플릿 함수
//*
//*
//*****************************************************************************
function message_omg( $blaha, $title=false ){
	// 제목이 따로 존재하지 않을 경우
	if( !$title ) { $title = "OMG!"; }
	echo '<h1>'.$title.'</h1>'.$blaha;
}



//*****************************************************************************
//*
//*
//*   플러그인 클래스( 추상화 )
//*
//*
//*****************************************************************************
class Plugin{
	public $NAME = null;
	public $CAPTION = null;
	public $STATUS = null;
	public $VERSION = null;
	public $DEVELOPER = null;
	
	function __construct( $install_file_path ){
		// 공통 이름
		$this->NAME =		conf_get("name")		? conf_get("name")		: "Unknown";
		$this->CAPTION =	conf_get("caption")		? conf_get("caption")	: "Unknown";
		$this->STATUS =		conf_get("status")		? conf_get("status")	: "Unknown";
		$this->VERSION =	conf_get("version")		? conf_get("version")	: "Unknown";
		$this->DEVELOPER = 	conf_get("developer")	? conf_get("developer") : "Unknown";
	}
}

function Plugin_Install( $plugin_object ){
	$Object = new $plugin_object();
}



//*****************************************************************************
//*
//*
//*   데이터베이스 클래스( SQLite 레퍼 )
//*
//*
//*****************************************************************************
//=========================================================
//# class; 데이터베이스 클래스
//=========================================================
class Database{
    // 내부 변수 정의
    private $PATH = null;
    private $TYPE = null;
    private $OBJECT = null;
	private $SELECTED_TABLE = null;
    
    //---------------------------------------------------------
    // 생성자
    //---------------------------------------------------------
    function __construct( $database_path, $database_type ){
		$this->PATH = $database_path; unset($database_path);
        $this->SetDatabaseType( $database_type );
    }

    //---------------------------------------------------------
    // 데이터베이스 타입
    //---------------------------------------------------------
    function SetDatabaseType( $database_type ){
        // 데이터베이스 타입 결정
        $this->TYPE = $database_type;
		
        // 전에 있던 데이터베이스 오브젝트 초기화
        $this->OBJECT = null;
        
        // 데이터베이스 타입이 정해짐과 동시에 SQLite 초기화 시작
        switch( $database_type ){
            //
            // SQLite3의 경우
            case 3:
                $this->OBJECT = new SQLite3( $this->PATH );
                break;
            //
            // SQLite2의 경우
            case 2:
                $this->OBJECT = sqlite_open( $this->PATH );
                break;
            //
            // ???의 경우
            default:
                die("<h1>OMG!</h1>데이터베이스 생성 중에 문제가 발생했습니다. (T_T)");
        }
    }
    
    //---------------------------------------------------------
    // 새 테이블 생성( 테이블 이름 , 옵션 쿼리 )
    // [ex] Database::NewTable("hello", "world varchar(20)");
    //---------------------------------------------------------
    function NewTable( $table_name, $option_query, $pass_if_exist_table_name = true ){
		// 테이블이 겹칠 경우 추가 쿼리
		$QueryForExistTable = "IF NOT EXISTS "; if( !$pass_if_exist_table_name ){ $QueryForExistTable=""; }
		
		// 단순 쿼리 실행
        $this->Put( "CREATE TABLE ".$QueryForExistTable.$table_name."(".$option_query.")" );
		
		// 테이블 이름 자동 선택
		$this->SELECTED_TABLE = $table_name;
    }
	
    //---------------------------------------------------------
    // 테이블 선택( 테이블 이름 )
    // [ex] Database::SelectTable("hello");
    //---------------------------------------------------------
    function SelectTable( $table_name ){
		$this->SELECTED_TABLE = $table_name;
	}
	
    //---------------------------------------------------------
    // 쿼리 실행( 단순한 쿼리실행, 리턴 값 없음. )
    //---------------------------------------------------------
    function Put( $string_query ){
        $Query = $string_query;
        // 데이터베이스 오브젝트가 살아있는 지 확인
        if( $this->OBJECT ){
            switch($this->TYPE){
                //
                // SQLite3의 경우
                case 3:
                    $this->OBJECT->exec( $Query );
                    break;
                //
                // SQLite2의 경우
                case 2:
                    sqlite_exec($this->OBJECT, $Query);
                    break;
            }
        }
        else{ die("<h1>OMG!</h1>database did lie!"); exit(); }
        
    }

	//---------------------------------------------------------
    // 복잡 쿼리 실행( 문자열 쿼리문, 리턴 값 타입 ) ;SQLiteResult가 리턴 값
	// [ex] Query( 'INSERT INTO table(1)' );
    //---------------------------------------------------------
    function Query( $string_query, $is_single = false ){
		// 쿼리 값
		$Query = $string_query; unset( $string_query );
        // 데이터베이스 오브젝트가 살아있는 지 확인
        if( $this->OBJECT ){
            switch($this->TYPE){
                //
                // SQLite3의 경우
                case 3:
					//
					// 싱글쿼리의 경우
					if( $is_single ){
						$ResultObject = $this->OBJECT->querySingle( $Query );
						return $ResultObject;
					}
					//
					// 기본 쿼리의 경우
					else {
						$ResultObject = $this->OBJECT->query( $Query );
						return $ResultObject;
					}
				break;
                //
                // SQLite2의 경우
                case 2:
					//
					// 싱글 커리의 경우
					if( $is_single ){
						$ResultObject = sqlite_single_query( $this->OBJECT, $Query );
						return $ResultObject;
					}
					//
					// 기본 쿼리의 경우
					else{
						$ResultObject = $this->OBJECT->sqlite_query( $this->OBJECT, $Query );
						return $ResultObject;
					}
				break;
            }
        }
        else{ die("<h1>OMG!</h1>database did lie!"); exit(); }
        
    }

    //---------------------------------------------------------
    // 값 추가
	// [ex] Database::InsertValue( "1, 'title', 'body'" )
    //---------------------------------------------------------
    function InsertValues( $option_query ){
        $this->Query("INSERT INTO ".$this->SELECTED_TABLE." VALUES(".$option_query.")");
    }
    
    //---------------------------------------------------------
    // 검색
    //---------------------------------------------------------
    function SearchAll( $where, $other_table_name = false ){
		// 다른 테이블이 아니라면, 기본 테이블을 선택한다.
		$TableName = $this->SELECTED_TABLE; if( $other_table_name ){ $TableName = $other_table_name; }
		//ID 설정 & 리턴할 배열 설정
		$IdNumber = 0; $DataArray = array();
		// 타입:
		// SQLITE_ASSOC, SQLITE3_ASSOC: 연관배열
		// SQLITE_NUM, SQLITE3_NUM: 배열
		// SQLITE_BOTH, SQLITE3_BOTH 둘 다
		
		// 타입이 존재하는 지 검사
		if( $this->TYPE ){
			// 데이터베이스 타입에 따라 쿼리 실행
			switch( $this->TYPE ){
				//
				// SQLite3의 경우
				case 3:
					// 쿼리 실행
					$ResultObject = $this->Query( "SELECT ".$where." FROM ".$TableName );
					while( $ArrayInResultObject = $ResultObject->fetchArray( SQLITE3_ASSOC ) ){
						// 배열 담기 & ID 번호 층가
						$DataArray[$IdNumber] = $ArrayInResultObject;
						$IdNumber++;
					}
					// 최종적으로 만든 데이터배열 리턴
					return $DataArray;
				break;
				
				//
				// SQLite2의 경우
				case 2:
					// 쿼리 실행
					$ResultObject = $this->Query( "SELECT ".$where." FROM ".$TableName );
					// 리턴 오브젝트를 핸들로 fetch 실행
					$ArrayInResultObject = sqlite_fetch_all( $ResultObject, SQLITE_ASSOC);
					// 배열로 담기 & ID 번호 증가
					foreach( $ArrayInResultObject as $Array ){
						$DataArray[$IdNumber] = $Array; $IdNumber++;
					}
					// 최종적으로 만든 데이터배열 리턴
					return $DataArray;
				break;
			}
		} else { die("<h1>OMG!</h1>database did lie!"); }
    }



    //---------------------------------------------------------
    // 검색
    //---------------------------------------------------------
    function Get( $where, $other_table_name = false ){
		// 다른 테이블이 아니라면, 기본 테이블을 선택한다.
		$TableName = $this->SELECTED_TABLE;
		if( $other_table_name ){ $TableName = $other_table_name; }

		// 타입이 존재하는 지 검사
		if( $this->TYPE ){
			// 데이터베이스 타입에 따라 쿼리 실행
			switch( $this->TYPE ){
				//
				// SQLite3의 경우
				case 3:
					//
					// 쿼리 실행( 주의! 싱글 쿼리임! )
					$DataArray = $this->Query( "SELECT ".$where." FROM ".$TableName, true );
					return $DataArray;
				break;
				
				//
				// SQLite2의 경우
				case 2:
					// 쿼리 실행
					$ResultObject = $this->Query( "SELECT ".$where." FROM ".$TableName );
					// 리턴 오브젝트를 핸들로 fetch 실행
					$ArrayInResultObject = sqlite_fetch_all( $ResultObject, SQLITE_ASSOC);
					// 배열로 담기 & ID 번호 증가
					foreach( $ArrayInResultObject as $Array ){
						$DataArray[$IdNumber] = $Array; $IdNumber++;
					}
					// 최종적으로 만든 데이터배열 리턴
					return $DataArray;
				break;
			}
		} else { die("<h1>OMG!</h1>database did lie!"); }
    }
	
	
	
    //---------------------------------------------------------
    // 해제
    //---------------------------------------------------------
    function Close(){
        // 데이터베이스 오브젝트가 살아있는 지 확인
        if( $this->OBJECT ){
            switch($this->TYPE){
                //
                // SQLite3의 경우
                case 3:
                    $this->OBJECT->Close();
                    break;
                //
                // SQLite2의 경우
                case 2:
                    sqlite_close($this->OBJECT);
                    break;
            }
        }
        else{ die("<h1>OMG!</h1>database did lie!"); exit(); }
    }
}



//*****************************************************************************
//*
//*
//*   설정화일 조작 관련 함수들
//*
//*
//*****************************************************************************
//=========================================================
//# func; 설정 화일 신규
// $KeyAndValue 매개변수 문자열 서식: (string)"$key:$value"
//=========================================================
function conf_new( $ConfFileName, $KeyAndValue ){

	// $KeyAndValue 패턴 서식 지정(귀찮아서 나중에 다시 하기로...)
	//$KeyAndValuePatten = "[.+]\:[.+]";
	// 서식에 맞지 않다면 작성을 취소한다.
	//if($KeyAndValue){  }
	
	// 전에 화일이 있었다면 지우고 시작한다.
	if(file_exists($ConfFileName)){ unlink($ConfFileName);}
	
	// 작성
	file_put_contents( $ConfFileName, $KeyAndValue );
}



//=========================================================
//# func; 설정 화일 내용 추가
//=========================================================
function conf_add( $ConfFileName, $KeyAndValue ){
	// 설정 화일을 읽고 덧붙이는 내용과 함께 다시 신규로 작성한다.
	if( file_exists($ConfFileName) ){
		$data=file_get_contents($ConfFileName);unlink($ConfFileName);
		conf_new($ConfFileName,$data."\n".$KeyAndValue);
	} else { die("설정화일을 찾을 수 없습니다."); }
}



//=========================================================
//# func; 설정 화일에서 Value 구하기
//=========================================================
function conf_get( $ConfFileName, $Key ){
	// 파일이 있다면 읽기 시작
	if( file_exists($ConfFileName) ){
		// 개행으로 구분. ex)'Key1:Value1', 'Key2:Value2', 'Key3:Value3'
		$DataArray = split( "\n", file_get_contents($ConfFileName) );
		
		// 검색 시작
		foreach( $DataArray as &$x ){
			// :로 구분. ex) 'Key', 'Value'
			$Key_Value = split(":", $x);
			// 찾는 key 값과 동일다면 value를 리턴
			if( $Key_Value[0] == $Key ){ return $Key_Value[1]; exit(); }
		}
		
		// 찾을 수 없다면 False
		return false;
	} else { die("설정 화일을 찾을 수 없습니다."); }
}



//*****************************************************************************
//*
//*
//*   컴포넌트 규격
//*
//*
//*****************************************************************************
class Component{
}



//*****************************************************************************
//*
//*
//*   필터 함수들
//*
//*
//*****************************************************************************
//=========================================================
//# func; 기본 필터
//=========================================================
function filter( $before_string, $remove_tag=true, $remove_sql=false){
	$after_string = $before_string;
	
	// 모든 태그 제거
	if( $remove_tag ){
		// 미구현
		return false;
	}
}

function filter_tag(){
	// 미구현
	return false;
}



//*****************************************************************************
//*
//*
//*   유틸리티 클래스, 함수
//*
//*
//*****************************************************************************
//=========================================================
// func; 템플릿 토큰 얻기
//=========================================================
function template_get_tokens( $html_body, $with_block=false ){
	// 먼저 선언
	$ResultArray = false; $Number = 0;
	// {{}} 와 같은 함정 문자열은 먼저 제거한다.
	$TemplateBody = str_replace( "{{}}", "", $html_body ); unset($html_body);
	
	// 파싱
	while( strpos($TemplateBody, "{{") ){
		// {{ 위치 찾기
		$StartPosition = strpos( $TemplateBody, "{{" );
		
		// }} 위치 찾기
		$EndPosition = strpos( substr($TemplateBody, $StartPosition), "}}" );
		
		// }}가 없으면 false
		if( $EndPosition == false ) { return false; break; }
		// }}가 있다면 위치에서 +2를 한다. '}}'가 두 문자이므로.
		if( $EndPosition == true  ) { $EndPosition+=2; }
		
		// {{와 }} 서로의 위치가 옮바른가?
		// ***{{와 달리 }}는 StartPosition부분부터 찾은 위치라 StartPosition이 커야만 맞다.
		if( $StartPosition > $EndPosition ){
			// 일치한다면 파싱한다. TokenWord는 {{var_name}} 형태.
			$TokenWord = substr($TemplateBody, $StartPosition, $EndPosition );
			
			// 찾은 토큰은 Body에서 제거한다. 그렇지 않으면 무한루프에 빠질 수도 있음.
			// 이를 통해 따로 배열에서 중복요소를 정리하지 않아도 된다.
			$TemplateBody = str_replace( $TokenWord, "", $TemplateBody );
			
			// 블록의 여부에 따라 배열로 담고 다음으로 넘긴다.
			if( $with_block == true ){
				$ResultArray[$Number] = $TokenWord; 
			} 
			else if ( $with_block == false ){
				// 얖 옆의 {{와 }}를 제거하고 담는다.
				$ResultArray[$Number] = substr( $TokenWord, 2, -2 );
			}
			++$Number;
		} else { return false; break; }
	}
	// 최종적으로 파싱이 실행되지 않는다면 false 값, 실행되었다면 TokenWord 배열이 리턴된다.
	return $ResultArray;
}

//=========================================================
// func; 템플릿 렌더링
//=========================================================
function template_render( $file_path, $assoc_array=true ){
	$Message = "<h1>Zi TemplateError</h1>";
	// 전달된 데이터가 존재하고, 배열인 지?
	if( $assoc_array and gettype($assoc_array)=="array" ){
		// 파일이 있나?
		if( file_exists($file_path) ){
			$TemplateBody = file_get_contents( $file_path );
			// 토큰 단어들 얻기
			$TokenWords = template_get_tokens( $TemplateBody, false );
			// 토큰이 없으면 그냥 렌더링 한다.
			if( $TokenWords == false ){ debug_echo("토큰 없음."); echo( $TemplateBody ); }
			else{
				// 토큰 배열과 데이터의 Key배열을 동기화(교집합) 한다.
				$TargetArray = array_intersect( $TokenWords, array_keys($assoc_array) );
				foreach( $TargetArray as $target ){
					$TemplateBody = str_replace( "{{".$target."}}", $assoc_array[$target], $TemplateBody );
				}
				// 치환 후 렌더링.
				echo( $TemplateBody );
			}
		}
		// 템플릿 파일이 없다면..
		else { echo($Message); echo("Can't find template file:");echo($file_path);exit(); }
	}
	// 잘못된 렌더링 데이터의 경우
	else {
		echo($Message); echo("Can't renderring the data:");
		echo(gettype($assoc_array)); exit();
	}
}



//=========================================================
// func; HTML 렌더링
//=========================================================
function html_render( $file_path ){
	echo( file_get_contents($file_path) );
}



//=========================================================
// func; 경로 알아내기
//=========================================================
function get_path( $with_filename=false ){
	if( $with_filename ){
		return realpath(__FILE__);
	} else {
		return str_replace(basename(__FILE__), "", realpath(__FILE__) );
	}
}
//*****************************************************************************
//*
//*
//*   유틸리티 클래스, 함수
//*
//*
//*****************************************************************************
//=========================================================
//# class; .htacess 접근 관련 유틸리티 클래스
//=========================================================
class Apache_htacess{
	function __construct(){
		return false;
	}
	
	function put(){
		return false;
	}
}
?>